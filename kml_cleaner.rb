require 'set'

def reduce(path)
geo_position = Set.new
count = 0

File.readlines(path).each do |line|
  coord = line.match /<gx:coord>([\d.]+ [\d.]+) [\d.]+/
  if coord
    count+= 1
    geo_position.add coord[1]
  end
end
  puts "count: #{count}"
  puts geo_position.length
File.open('./sample/clean.kml','w') do |f|
  f.write("<?xml version='1.0' encoding='UTF-8'?>
<kml xmlns='http://www.opengis.net/kml/2.2' xmlns:gx='http://www.google.com/kml/ext/2.2'>
  <Document>
    <Placemark>
      <open>1</open>
      <gx:Track>\n")

  geo_position.each do |geo|
    f.write("        <gx:coord>#{geo} 0</gx:coord>\n")
  end

  f.write("      </gx:Track>
    </Placemark>
  </Document>
</kml>\n")

end

end

reduce('./sample/LocationHistory.kml')

#reduce('./sample/ReducedLocationHistory.kml')